<?php
/**
 * @file
 * ApnsPHP_Exception class definition.
 */

/**
 * Exception class.
 *
 * @ingroup ApplePushNotificationService
 */
class ApnsPHP_Exception extends Exception
{
}