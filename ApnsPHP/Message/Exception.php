<?php
/**
 * @file
 * ApnsPHP_Message_Exception class definition.
 */

/**
 * Exception class.
 *
 * @ingroup ApnsPHP_Message
 */
class ApnsPHP_Message_Exception extends ApnsPHP_Exception
{
}