<?php
/**
 * @file
 * ApnsPHP_Push_Server_Exception class definition.
 */

/**
 * Exception class.
 *
 * @ingroup ApnsPHP_Push_Server
 */
class ApnsPHP_Push_Server_Exception extends ApnsPHP_Push_Exception
{
}