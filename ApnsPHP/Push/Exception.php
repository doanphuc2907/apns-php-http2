<?php
/**
 * @file
 * ApnsPHP_Push_Exception class definition.
 */

/**
 * Exception class.
 *
 * @ingroup ApnsPHP_Push
 */
class ApnsPHP_Push_Exception extends ApnsPHP_Exception
{
}